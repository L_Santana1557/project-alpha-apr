from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import Project_Create_Form

# Create your views here.


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = Project_Create_Form(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = Project_Create_Form()
    context = {"form": form}

    return render(request, "projects/create.html", context)
