from django.forms import ModelForm
from projects.models import Project


class Project_Create_Form(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]
