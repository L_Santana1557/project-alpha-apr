from django.shortcuts import render, redirect
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from tasks.forms import Task_Create_Form

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = Task_Create_Form(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = Task_Create_Form()
    context = {"form": form}

    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "tasks": tasks,
    }
    return render(request, "tasks/list.html", context)
